﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RunAsp
{
    /// <summary>
    /// Interaction logic for InputUser.xaml
    /// </summary>
    public partial class InputUser : Window
    {

        public MainWindow mainwindow;
        public InputUser(MainWindow mainwindow)
        {
            InitializeComponent();
            this.mainwindow = mainwindow;
            this.User.Text = (string)mainwindow.Settings["User"];
            this.Password.Text = mainwindow.Decrypt((string)mainwindow.Settings["Pass"]);
        }

        private void UpdateUserPass(object sender, RoutedEventArgs e)
        {
            mainwindow.SaveSetting("User", this.User.Text);
            mainwindow.SaveSetting("Pass", mainwindow.Encrypt(this.Password.Text));
            mainwindow.startISSUser(this);
        }
    }
}
